This is a short guide to running the Fast Microsatellite Discovery software
In the Linux command terminal go to the path where FMSD is there and type the a command similar to the line below.

./FMSD  –Min_size  2  –Max_size  6  –input data.fasta

The parameters of the above command are as follows
1.	Min_size is the minimum size of the core subsequence of the satellites of your interest.
	 For example, in the above command the user is interested in microsatellites of core sizes 2 to 6 hence this parameter is taken to be 2.
	 The least value of this parameter is 1.

2.	 Max_ size is the maximum size of the core subsequence of the satellites of your interest.
	 For example, in the above command the user is interested in microsatellites of core sizes 2 to 6 hence this parameter is taken to be 6. 
	 The highest value of this parameter is 7.

The output is a text file composed of three columns:
1.	core shows the core nucleotide subsequence
2.	Start_loc is the starting location of the microsatellite
3.	Repeat is the number of repeats of the microsatellite.
